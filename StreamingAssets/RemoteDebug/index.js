let $floatingWindowContent = null;
let $floatingWindowOpenBtn = null;
let $floatingWindowCloseBtn = null;
let $input = null;
let $output = null;
let $updateOutput = null;

let commandIndex = -1;
let updateOut = true;
let updateIntervalId = null;

const ENTER_KEYCODE = 13;
const UP_KEYCODE = 38;
const DOWN_KEYCODE = 40;
const ESCAPE_KEYCODE = 27;
const TAB_KEYCODE = 9;

const openFloatingWindow = () => {
    $floatingWindowContent.show();
    $floatingWindowOpenBtn.hide();
    $floatingWindowCloseBtn.show();
}

const closeFloatingWindow = () => {
    $floatingWindowContent.hide();
    $floatingWindowOpenBtn.show();
    $floatingWindowCloseBtn.hide();
}

const scrollToBottom = () => {
    $output.scrollTop($output[0].scrollHeight);
}

const resetInput = () => {
    commandIndex = -1;
    $input.val("");
}

const updateCommand = index => {
    if (index < 0) {
        resetInput();
        return;
    }

    updateCommandRequest(index);
}

const startUpdateInterval = () => {
    if (updateOut) {
        console.log("StartUpdateInterval");
        updateIntervalId = window.setInterval(() => { updateConsole(null); }, 1000);
    }
}

const setUpdateOutput = value => {
    updateOut = value;
    if (updateOut) {
        startUpdateInterval();
    } else if (updateIntervalId != null) {
        window.clearInterval(updateIntervalId);
        updateIntervalId = null;
    }
}

$(document).ready(() => {
    $floatingWindowContent = $("#windowContent");
    $floatingWindowOpenBtn = $("#openWindowBtn");
    $floatingWindowCloseBtn = $("#closeWindowBtn");
    $input = $("#input");
    $output = $("#output");
    $updateOutput = $("#updateOutput");

    // TODO: Could use only one button and change the class by code instead of change display

    if (!$floatingWindowContent || !$floatingWindowOpenBtn || !$floatingWindowCloseBtn || !$input || !$output) {
        alert("Could not get necessary elements");
    }

    closeFloatingWindow();
    $floatingWindowOpenBtn.click(() => openFloatingWindow());
    $floatingWindowCloseBtn.click(() => closeFloatingWindow());

    $input.keydown(e => {
        const command = $input.val();
        if (e.keyCode == ENTER_KEYCODE) {
            e.preventDefault();
            runCommand(command);
        } else if (e.keyCode == UP_KEYCODE) {
            updateCommand(commandIndex + 1);
        } else if (e.keyCode == DOWN_KEYCODE) {
            updateCommand(commandIndex - 1);
        } else if (e.keyCode == ESCAPE_KEYCODE) {
            resetInput();
        } else if (e.keyCode == TAB_KEYCODE) {
            e.preventDefault();
            completeCommand(command);
        }
    });

    $updateOutput.click(() => setUpdateOutput($updateOutput.is(':checked')));

    startUpdateInterval();
    $input.focus();
});
