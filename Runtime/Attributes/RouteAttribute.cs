﻿using System;
using System.Text.RegularExpressions;

namespace RemoteDebug.Attributes
{
	[AttributeUsage(AttributeTargets.Method)]
	public class RouteAttribute : Attribute
	{
		public delegate void Callback(RequestContext ctx);

		public bool IsValid => Route != null;
		public Callback Cbk { get; set; } = null;

		public Regex Route { get; } = default;
		public Regex Methods { get; } = default;
		public bool RunOnMainThread { get; } = default;

		public RouteAttribute(string rout, string methods = @"(GET|HEAD)", bool runOnMainThread = true)
		{
			Route = new Regex(rout, RegexOptions.IgnoreCase);
			Methods = new Regex(methods);
			RunOnMainThread = runOnMainThread;
		}
	}
}
