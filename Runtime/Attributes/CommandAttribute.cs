﻿using System;

namespace RemoteDebug.Attributes
{
	[AttributeUsage(AttributeTargets.Method, AllowMultiple = true)]
	public class CommandAttribute : Attribute
	{
		public delegate void EmptyCallback();
		public delegate void ConsoleParamCallback(Console.Console console);
		public delegate void StringParamCallback(string[] args);
		public delegate void Callback(Console.Console console, string[] args);
		
		public string Command { get; } = default;
		public string Help { get; } = default;
		public bool RunOnMainThread { get; } = default;
		public Callback Cbk;

		public CommandAttribute(string cmd, string help, bool runOnMainThread = true)
		{
			Command = cmd;
			Help = help;
			RunOnMainThread = runOnMainThread;
		}
	}
}
