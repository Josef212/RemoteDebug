﻿using System;
using System.Collections.Generic;
using System.Text;
using RemoteDebug.Attributes;
using UnityEngine;

namespace RemoteDebug.Console
{
	public class Console
	{
		internal struct QueuedCommand
		{
			public CommandAttribute Command;
			public string[] Args;
		}

		private const int _maxLines = 100;
		private const int _maxHistory = 50;
		private const string _outputPrefix = "> ";
		
		private ICommandsContainer _commandsContainer = default;
		private List<string> _output = default;
		private List<string> _history = default;
		private Queue<QueuedCommand> _commandsQueue = default;

		public ICommandsContainer Container => _commandsContainer;

		public Console()
		{
			_commandsContainer = new CommandsContainer();
			_output = new List<string>();
			_history = new List<string>();
			_commandsQueue = new Queue<QueuedCommand>();
		}

		public void Update()
		{
			while (_commandsQueue.Count > 0)
			{
				var cmd = _commandsQueue.Dequeue();
				cmd.Command.Cbk(this, cmd.Args);
			}
		}

		public void Clear()
		{
			_output.Clear();
		}

		public void LogHelp()
		{
			var sb = new StringBuilder();

			void LogCommandHelp(CommandAttribute cmd)
			{
				sb.Append($"  {cmd.Command}: {cmd.Help}\n");
			}
			
			_commandsContainer.ForEach(LogCommandHelp);
			Log($"<span class='Help'>{sb}</span>");
		}

		public void Info(string log)
		{
			Log(log);
		}
		
		public void Warn(string log)
		{
			Log($"<span class='Warning'>{log}</span>");
		}
		
		public void Error(string log)
		{
			Log($"<span class='Error'>{log}</span>");
		}

		public void Help(string log)
		{
			Log($"<span class='Help'>{log}</span>");
		}

		public string GetOutput()
		{
			return string.Join("\n", _output.ToArray());
		}

		public string GetPreviousCommand(int index)
		{
			return index >= 0 && index < _history.Count ? _history[index] : string.Empty;
		}

		public void RunCommand(string input)
		{
			if (string.IsNullOrEmpty(input))
			{
				return;
			}

			LogCommand(input);
			RecordCommand(input);
			var result = _commandsContainer.GetCommand(input);
			RunCommand(result);
		}

		public void LogCallback(string log, string stackTrace, LogType logType)
		{
			// TODO: This assumes the end user is using html, could not rely on that
			if (logType != LogType.Log)
			{
				Log($"<span class='{logType}'>{log}");
				Log($"{stackTrace}</span>");
			}
			else
			{
				Log(log);
			}
		}

		public void Log(string log)
		{
			_output.Add(log);
			if (_output.Count > _maxLines)
			{
				_output.RemoveAt(0); // TODO: Could do it better and avoid removing at 0. Instead using a queue maybe?
			}
		}

		private void LogCommand(string cmd)
		{
			Log($"{_outputPrefix}{cmd}");
		}

		private void RecordCommand(string cmd)
		{
			_history.Insert(0, cmd);
			if (_history.Count > _maxHistory)
			{
				_history.RemoveAt(_history.Count - 1);
			}
		}

		private void RunCommand(Tuple<CommandAttribute, string[]> result)
		{
			if (result?.Item1 == null)
			{
				Log("Command not found...");
				return;
			}

			var cmd = result.Item1;
			var args = result.Item2;

			if (cmd.RunOnMainThread)
			{
				_commandsQueue.Enqueue(new QueuedCommand()
				{
					Command = cmd,
					Args = args
				});
			}
			else
			{
				cmd.Cbk(this, args);
			}
		}
	}
}
