﻿using System;
using System.Collections.Generic;
using RemoteDebug.Attributes;
using UnityEngine;

namespace RemoteDebug.Console
{
	internal class CommandsContainerGroup
	{
		private const char _separatorChar = '.';
		private static readonly char[] _separators = { _separatorChar };
		private static readonly string[] _emptyArgs = new string[0] { };

		private Dictionary<string, CommandAttribute> _commands = default;
		private Dictionary<string, CommandsContainerGroup> _subGroups = default;
		
		public string GroupKey { get; } = default;

		internal CommandsContainerGroup(string groupKey)
		{
			GroupKey = groupKey;
			_commands = new Dictionary<string, CommandAttribute>();
			_subGroups = new Dictionary<string, CommandsContainerGroup>();
		}

		internal void AddCommand(CommandAttribute commandAttribute)
		{
			if (commandAttribute == null)
			{
				return;
			}

			var name = commandAttribute.Command;
			SplitCommandWithoutArgs(name, out var splitCommand);
			AddCommand(commandAttribute, splitCommand, 0);
		}

		internal bool RemoveCommand(CommandAttribute commandAttribute)
		{
			if (commandAttribute == null)
			{
				return true;
			}
			
			if (_commands.ContainsKey(commandAttribute.Command))
			{
				_commands.Remove(commandAttribute.Command);
				return true;
			}

			foreach (var group in _subGroups.Values)
			{
				if (group.RemoveCommand(commandAttribute))
				{
					return true;
				}
			}

			return false;
		}

		internal Tuple<CommandAttribute, string[]> GetCommand(string commandName)
		{
			if (string.IsNullOrEmpty(commandName))
			{
				return null;
			}

			SplitCommandWithArgs(commandName, out var splitCommandName, out var args);
			var command = GetCommand(splitCommandName, 0);
			return new Tuple<CommandAttribute, string[]>(command, args);
		}

		internal void GetMatchingCommands(string input, ref List<string> list)
		{
			void AddGroupCommands(string inp, bool force, ref List<string> lst)
			{
				foreach (var command in _commands)
				{
					if (force || command.Key.StartsWith(inp))
					{
						lst.Add(command.Value.Command);
					}
				}
			}
			
			if (GroupKey.StartsWith(input))
			{
				if (GroupKey != input)
				{
					list.Add($"{GroupKey}.");
				}
				else
				{
					AddGroupCommands(input, true, ref list);
				}
			}

			AddGroupCommands(input, false, ref list);

			foreach (var subGroup in _subGroups.Values)
			{
				subGroup.GetMatchingCommands(input, ref list);
			}
		}

		internal void ForEach(Action<CommandAttribute> cbk)
		{
			foreach (var command in _commands)
			{
				cbk.Invoke(command.Value);
			}

			foreach (var group in _subGroups)
			{
				group.Value.ForEach(cbk);
			}
		}

		private void AddCommand(CommandAttribute commandAttribute, string[] splitCommandName, int index)
		{
			if (splitCommandName.Length == 0 || splitCommandName.Length <= index)
			{
				return;
			}

			if (splitCommandName.Length - 1 == index)
			{
				var commandName = splitCommandName[index];

				if (string.IsNullOrEmpty(commandName))
				{
					Debug.LogError("Trying to add invalid command name.");
					return;
				}
				
				if (_commands.ContainsKey(commandName))
				{
					Debug.LogError($"Trying to add duplicated command: {commandName}");
					return;
				}

				_commands.Add(commandName, commandAttribute);
				return;
			}

			var groupName = splitCommandName[index];
			if (string.IsNullOrEmpty(groupName))
			{
				Debug.LogError("Trying to add command with invalid group name.");
				return;
			}

			if (!_subGroups.TryGetValue(groupName, out var group))
			{
				group = new CommandsContainerGroup(groupName);
				_subGroups.Add(groupName, group);
			}
			
			group.AddCommand(commandAttribute, splitCommandName, index + 1);
		}

		private CommandAttribute GetCommand(string[] splitCommand, int index)
		{
			if (splitCommand.Length == 0)
			{
				return null;
			}

			if (splitCommand.Length - 1 == index)
			{
				var name = splitCommand[index];
				if (string.IsNullOrEmpty(name))
				{
					return null;
				}

				if (!_commands.TryGetValue(name, out var command))
				{
					Debug.LogError($"Command not found: {name}");
					return null;
				}

				return command;
			}

			var groupName = splitCommand[index];
			if (string.IsNullOrEmpty(groupName))
			{
				Debug.LogError("Invalid group name.");
				return null;
			}

			if (!_subGroups.TryGetValue(groupName, out var group))
			{
				group = new CommandsContainerGroup(groupName);
			}
			
			return group.GetCommand(splitCommand, index + 1);
		}

		private void SplitCommandWithoutArgs(string input, out string[] splitCommands)
		{
			splitCommands = input.Split(_separators);
		}
		
		private void SplitCommandWithArgs(string input, out string[] splitCommands, out string[] args)
		{
			splitCommands = input.Split(_separators);
			var lastIndex = splitCommands.Length - 1;
			var commandSpaceIndex = splitCommands[lastIndex].IndexOf(' ');
			if (commandSpaceIndex != -1)
			{
				var lastCommand = splitCommands[lastIndex];
				splitCommands[lastIndex] = lastCommand.Substring(0, commandSpaceIndex);
				var argsStr = lastCommand.Substring(commandSpaceIndex + 1);
				args = SplitArgs(argsStr);
			}
			else
			{
				args = _emptyArgs;
			}
		}

		private string[] SplitArgs(string args)
		{
			static bool IsQuotes(char c) => c == '"' || c == '\'';
			
			var ret = new List<string>();
			
			var enteredArg = false;
			var enteredQuotesArg = false;
			var argStartIndex = 0;
			
			for (var i = 0; i < args.Length; i++)
			{
				var c = args[i];
				var isQuotes = IsQuotes(c);
				var isSpace = c == ' ';

				if (isSpace && !enteredQuotesArg)
				{
					// Arg completed
					ret.Add(args.Substring(argStartIndex, i - argStartIndex));
					enteredArg = false;
					continue;
				}

				if (isQuotes && enteredArg)
				{
					// Arg completed
					ret.Add(args.Substring(argStartIndex, i - argStartIndex));
					enteredArg = false;
					enteredQuotesArg = false;
					continue;
				}

				if (isQuotes && !enteredArg)
				{
					// Start arg
					argStartIndex = i + 1;
					enteredQuotesArg = true;
					enteredArg = true;
					continue;
				}

				if (!isSpace && !isQuotes && !enteredArg)
				{
					// Start arg
					argStartIndex = i;
					enteredArg = true;
				}
			}

			if (enteredArg)
			{
				ret.Add(args.Substring(argStartIndex, args.Length - argStartIndex));
			}

			return ret.ToArray();
		}

#if UNITY_EDITOR
		public void SplitCommandWithoutArgsTest(string input, out string[] splitCommands)
		{
			SplitCommandWithoutArgs(input, out splitCommands);
		}
		
		public void SplitCommandWithArgsTest(string input, out string[] splitCommands, out string[] args)
		{
			SplitCommandWithArgs(input, out splitCommands, out args);
		}
#endif
	}
}
