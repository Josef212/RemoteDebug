﻿using System;
using System.Collections.Generic;
using System.Reflection;
using RemoteDebug.Attributes;
using UnityEngine;

namespace RemoteDebug.Console
{
	internal class CommandsContainer : ICommandsContainer
	{
		private readonly CommandsContainerGroup _root;

		internal CommandsContainer()
		{
			_root = new CommandsContainerGroup("");
			RegisterCommands();
		}

		public void AddCommand(CommandAttribute command)
		{
			_root.AddCommand(command);
		}

		public void RemoveCommand(CommandAttribute command)
		{
			_root.RemoveCommand(command);
		}
		
		public Tuple<CommandAttribute, string[]> GetCommand(string commandStr)
		{
			return string.IsNullOrEmpty(commandStr) ? null : _root.GetCommand(commandStr);
		}

		public List<string> GetMatchingCommands(string input)
		{
			var ret = new List<string>();
			
			if (string.IsNullOrEmpty(input))
			{
				return ret;
			}
			
			SplitCommandWithoutArgsTest(input, out var splitCommands);

			if (splitCommands == null || splitCommands.Length == 0)
			{
				return ret;
			}

			var last = splitCommands[splitCommands.Length - 1];
			last = string.IsNullOrEmpty(last) ? splitCommands[splitCommands.Length - 2] : last;
			_root.GetMatchingCommands(last, ref ret);

			return ret;
		}
		
		public void ForEach(Action<CommandAttribute> cbk)
		{
			if (cbk == null)
			{
				return;
			}

			_root.ForEach(cbk);
		}

		private void RegisterCommands()
		{
			foreach (var assembly in AppDomain.CurrentDomain.GetAssemblies())
			{
				if (assembly.FullName.StartsWith("System") || assembly.FullName.StartsWith("mscorlib"))
				{
					continue;
				}

				foreach (var type in assembly.GetTypes())
				{
					RegisterTypeCommand(type);
				}
			}
		}

		private void RegisterTypeCommand(Type type)
		{
			bool TryCreateDelegate<T>(MethodInfo method, out T cbk) where T : Delegate
			{
				cbk = Delegate.CreateDelegate(typeof(T), method, false) as T;
				return cbk != null;
			}
			
			// TODO: Only static methods supported for now
			foreach (var methodInfo in type.GetMethods(BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.Static))
			{
				var attrs = methodInfo.GetCustomAttributes(typeof(CommandAttribute), true) as CommandAttribute[];
				if (attrs == null || attrs.Length == 0)
				{
					continue;
				}

				var parametersCount = methodInfo.GetParameters().Length;
				CommandAttribute.Callback cbk = null;
				if (parametersCount == 0)
				{
					if (TryCreateDelegate(methodInfo, out CommandAttribute.EmptyCallback tmp))
					{
						cbk = (console, args) => tmp();
					}
				}
				else if (parametersCount == 1)
				{
					if (TryCreateDelegate(methodInfo, out CommandAttribute.ConsoleParamCallback tmp))
					{
						cbk = (console, args) => tmp(console);
					}
					else if (TryCreateDelegate(methodInfo, out CommandAttribute.StringParamCallback tmp2))
					{
						cbk = (console, args) => tmp2(args);
					}
				}
				else if (parametersCount == 2)
				{
					if (TryCreateDelegate(methodInfo, out CommandAttribute.Callback tmp))
					{
						cbk = tmp;
					}
				}

				if (cbk == null)
				{
					Debug.LogError($"Method {type}. {methodInfo.Name} takes the wrong arguments for a console command.");
					continue;
				}

				foreach (var commandAttribute in attrs)
				{
					if (string.IsNullOrEmpty(commandAttribute.Command))
					{
						Debug.LogError($"Method {type}. {methodInfo.Name} needs a valid command name.");
						continue;
					}

					commandAttribute.Cbk = cbk;
					AddCommand(commandAttribute);
				}
			}
		}
		
#if UNITY_EDITOR
		public void SplitCommandWithoutArgsTest(string input, out string[] splitCommands)
		{
			_root.SplitCommandWithoutArgsTest(input, out splitCommands);
		}
		
		public void SplitCommandWithArgsTest(string input, out string[] splitCommands, out string[] args)
		{
			_root.SplitCommandWithArgsTest(input, out splitCommands, out args);
		}
#endif
	}
}
