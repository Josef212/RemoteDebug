﻿using System;
using System.Collections.Generic;
using RemoteDebug.Attributes;

namespace RemoteDebug.Console
{
	public interface ICommandsContainer
	{
		void AddCommand(CommandAttribute command);
		void RemoveCommand(CommandAttribute command);
		Tuple<CommandAttribute, string[]> GetCommand(string commandStr);
		List<string>  GetMatchingCommands(string input);
		void ForEach(Action<CommandAttribute> cbk);
#if UNITY_EDITOR
		void SplitCommandWithoutArgsTest(string input, out string[] splitCommands);
		void SplitCommandWithArgsTest(string input, out string[] splitCommands, out string[] args);
#endif
	}
}
