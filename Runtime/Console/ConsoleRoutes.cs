﻿using System;
using System.Text;
using RemoteDebug.Attributes;

namespace RemoteDebug.Console
{
	public static class ConsoleRoutes
	{
		public static RouteAttribute OutputRoute(Console console)
		{
			return new RouteAttribute("^/console/out$")
			{
				Cbk = ctx => ctx.Response.WriteString(console.GetOutput())
			};
		}

		public static RouteAttribute RunRoute(Console console)
		{
			void Method(RequestContext ctx)
			{
				var data = ctx.Request.QueryString.Get("command");
				var command = Uri.UnescapeDataString(data);
				if (!string.IsNullOrEmpty(command))
				{
					console.RunCommand(command);
				}
				
				ctx.SetOkResponse();
			}

			return new RouteAttribute("^/console/run$") { Cbk = Method };
		}

		public static RouteAttribute History(Console console)
		{
			void Method(RequestContext ctx)
			{
				var index = ctx.Request.QueryString.Get("index");
				var prev = string.Empty;
				
				if (!string.IsNullOrEmpty(index) && int.TryParse(index, out var iIndex))
				{
					prev = console.GetPreviousCommand(iIndex);
				}
				
				ctx.Response.WriteString(prev);
			}
			
			return new RouteAttribute("^/console/commandHistory") { Cbk = Method };
		}

		public static RouteAttribute Complete(Console console)
		{
			void Method(RequestContext ctx)
			{
				var partialCommand = ctx.Request.QueryString.Get("command");
				partialCommand = Uri.UnescapeDataString(partialCommand);
				var matchingCommands = console.Container.GetMatchingCommands(partialCommand);

				var sb = new StringBuilder();

				if (matchingCommands == null || matchingCommands.Count == 0)
				{
					sb.Append("  No matching commands found.\n");
				}
				else if (matchingCommands.Count == 1)
				{
					ctx.Response.WriteString(matchingCommands[0]);
				}
				else
				{
					sb.Append($"Matching commands for: {partialCommand}\n");
					sb.Append("<span class='Help'>");
					
					foreach (var command in matchingCommands)
					{
						sb.Append($"  {command}\n");
					}

					sb.Append("</span>");
					
					console.Log(sb.ToString());
					
					ctx.SetOkResponse();
				}
			}
			
			return new RouteAttribute("^/console/complete$") { Cbk = Method };
		}
	}
}
