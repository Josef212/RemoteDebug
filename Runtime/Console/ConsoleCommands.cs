﻿using RemoteDebug.Attributes;

namespace RemoteDebug.Console
{
	public static class ConsoleCommands
	{
		[Command("cls", "Clear the console output", false)]
		[Command("clear", "Clear the console output", false)]
		public static void Clear(Console console)
		{
			console.Clear();
		}

		[Command("help", "Prints commands list")]
		public static void Help(Console console)
		{
			console.LogHelp();
		}
	}
}
