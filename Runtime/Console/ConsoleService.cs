using System.Collections.Generic;
using RemoteDebug.Attributes;
using UnityEngine;

namespace RemoteDebug.Console
{
	// TODO: Probably should ensure somehow double instances and trying to have two consoles.. Although might be useful to simulate two instances :thinking:
	[CreateAssetMenu]
	public class ConsoleService : RemoteDebugService
	{
		private Console _console = null;
		private List<RouteAttribute> _routes;
		
		protected override IEnumerable<RouteAttribute> ServiceRoutes => _routes;
		public Console Console => _console;
		
		protected override void OnPreInitService()
		{
			_console ??= new Console();
			_routes ??= new List<RouteAttribute>()
			{
				ConsoleRoutes.OutputRoute(_console),
				ConsoleRoutes.RunRoute(_console),
				ConsoleRoutes.History(_console),
				ConsoleRoutes.Complete(_console)
			};
		}

		public override void PostInitService()
		{
			_remoteDebug.Events.OnUpdate += _console.Update;
			_remoteDebug.Events.OnLog += _console.LogCallback;
		}

		protected override void OnCleanUp()
		{
			_remoteDebug.Events.OnUpdate -= _console.Update;
			_remoteDebug.Events.OnLog -= _console.LogCallback;
		}
	}
}
