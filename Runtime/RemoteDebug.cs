﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Threading;
using RemoteDebug.Console;
using RemoteDebug.Server;
using UnityEngine;

namespace RemoteDebug
{
	public class RemoteDebug : MonoBehaviour
	{
		[SerializeField] private bool _dontDestroyOnLoad = true;
		[SerializeField] private string _filesRoot = "RemoteDebug";
		[SerializeField] private int _port = 55055;
		[SerializeField] private bool _registerLogCallback = true;
		
		[SerializeField] private RemoteDebugService[] _debugServices = default;

		private Dictionary<Type, RemoteDebugService> _activeServices = default;
		private RemoteDebugEvents _remoteDebugEvents = default;

		private IRemoteDebugRunner _runner = default;
		private IRoutesContainer _routesContainer = default;
		private IRemoteDebugConnection _connection = default;
		
		public IRoutesContainer RoutesContainer => _routesContainer;
		public RemoteDebugEvents Events => _remoteDebugEvents;
		
		public ConsoleService ConsoleService => GetRemoteService<ConsoleService>();

		public T GetRemoteService<T>(bool includeInactive = false) where T : RemoteDebugService
		{
			if (_activeServices.TryGetValue(typeof(T), out var ret))
			{
				return ret as T;
			}

			if (!includeInactive)
			{
				return default(T);
			}
			
			foreach (var service in _debugServices)
			{
				if (service == null || service.EnabledDefault)
				{
					continue;
				}

				if (service.GetType() == typeof(T))
				{
					return service as T;
				}
			}

			return default(T);
		}

		public bool ActivateRemoteService<T>() where T : RemoteDebugService
		{
			if (IsRemoteServiceActive<T>())
			{
				return false;
			}

			foreach (var service in _debugServices)
			{
				if (service.GetType() == typeof(T))
				{
					service.PreInitService(this, _routesContainer);
					service.PostInitService();

					return true;
				}
			}

			return false;
		}

		public bool DeactivateRemoteService<T>() where T : RemoteDebugService
		{
			if (!IsRemoteServiceActive<T>())
			{
				return false;
			}

			var serviceType = typeof(T);
			_activeServices[serviceType].CleanUpService();
			_activeServices.Remove(serviceType);

			return true;
		}

		public bool IsRemoteServiceActive<T>() where T : RemoteDebugService
		{
			return _activeServices.ContainsKey(typeof(T));
		}
		
		private void Init()
		{
			var root = Path.Combine(Application.streamingAssetsPath, _filesRoot);
			_activeServices = new Dictionary<Type, RemoteDebugService>();
			_remoteDebugEvents = new RemoteDebugEvents();

			_routesContainer = new RoutesContainer();
			_routesContainer.RegisterGlobalRoutes();
			FilesHandler.RegisterFileHandlers(root, _routesContainer);
			
			_runner = new RemoteDebugRunner(_routesContainer, Thread.CurrentThread);
			InstallServices();
			
			_connection = new HttpRemoteDebugConnection(_port, _runner.HandleRequest);
		}


		private void InstallServices()
		{
			if (_debugServices == null)
			{
				return;
			}
			
			_activeServices.Clear();
			
			foreach (var service in _debugServices)
			{
				if (service != null && service.EnabledDefault)
				{
					service.PreInitService(this, _routesContainer);
					_activeServices.Add(service.GetType(), service);
				}
			}

			foreach (var service in _activeServices)
			{
				service.Value.PostInitService();
			}
		}

		protected virtual void Awake()
		{
			if (_dontDestroyOnLoad)
			{
				DontDestroyOnLoad(gameObject);
			}
			
			Init();
		}

		protected virtual void OnDestroy()
		{
			foreach (var service in _activeServices)
			{
				service.Value.CleanUpService();
			}
			
			_activeServices.Clear();
			_connection.Close();
		}

		private void Update()
		{
			// TODO: Maybe should update once ore twice every second instead of every frame
			_runner.Run();
			_remoteDebugEvents.RaiseOnUpdate();
		}

		private void OnEnable()
		{
			if (_registerLogCallback)
			{
#if UNITY_5_3_OR_NEWER
				Application.logMessageReceived += _remoteDebugEvents.RaiseOnLog;
#else
				Application.RegisterLogCallback(_remoteDebugEvents.RaiseOnLog);
#endif
			}
		}

		private void OnDisable()
		{
			if (_registerLogCallback)
			{
#if UNITY_5_3_OR_NEWER
				Application.logMessageReceived -= _remoteDebugEvents.RaiseOnLog;
#else
				Application.RegisterLogCallback(null);
#endif
			}
		}

		private void OnApplicationPause(bool pauseStatus)
		{
			if (pauseStatus)
			{
				_connection.Stop();
			}
			else
			{
				_connection.Start();
			}
		}
	}
}
