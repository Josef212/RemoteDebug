﻿using System.Net;
using System.Text.RegularExpressions;
using UnityEngine.Networking;

namespace RemoteDebug
{
	public class RequestContext
	{
		public HttpListenerContext Context;
		public Match Match;
		public bool Pass;
		public string Path;

		public HttpListenerRequest Request => Context?.Request;
		public HttpListenerResponse Response => Context?.Response;

		public RequestContext(HttpListenerContext ctx)
		{
			Context = ctx;
			Match = null;
			Pass = false;
			Path = UnityWebRequest.UnEscapeURL(Request.Url.AbsolutePath);
			if (Path == "/")
			{
				Path = "/index.html";
			}
		}
	}
}
