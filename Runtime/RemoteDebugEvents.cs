using System;
using UnityEngine;

namespace RemoteDebug
{
	public sealed class RemoteDebugEvents
	{
		public event Action OnUpdate = () => { };
		public event Action<string, string, LogType> OnLog = (s, s1, arg3) => { };

		public void RaiseOnUpdate()
		{
			OnUpdate();
		}

		public void RaiseOnLog(string s, string s2, LogType log)
		{
			OnLog(s, s2, log);
		}
	}
}