﻿using System;
using System.ComponentModel;

namespace RemoteDebug
{
	public static class ArgumentsHelper
	{
		public static bool GetFlag(this string[] args, string flag)
		{
			foreach (var arg in args)
			{
				if(arg == flag)
				{
					return true;
				}
			}

			return false;
		}

		public static bool GetArgAt<T>(this string[] args, int index, out T argValue)
		{
			if (index >= 0 && index < args.Length)
			{
				var arg = args[index];
				try
				{
					argValue = (T)TypeDescriptor.GetConverter(typeof(T)).ConvertFrom(arg);
				}
				catch (Exception)
				{
					argValue = default;
					return false;
				}
						
				return true;
			}

			argValue = default;
			return false;
		}

		public static bool GetArgValue<T>(this string[] args, string argName, out T argValue)
		{
			for (int i = 0; i < args.Length; i++)
			{
				var arg = args[i];
				if (arg == argName)
				{
					if(i + 1 < args.Length)
					{
						var value = args[i + 1];
						try
						{
							argValue = (T)TypeDescriptor.GetConverter(typeof(T)).ConvertFrom(value);
						}
						catch (Exception)
						{
							argValue = default;
							return false;
						}
						
						return true;
					}
				}
			}

			argValue = default;
			return false;
		}
	}
}