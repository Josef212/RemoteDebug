﻿using System;
using System.Collections.Generic;
using System.Reflection;
using System.Text.RegularExpressions;
using RemoteDebug.Attributes;
using UnityEngine;

namespace RemoteDebug.Server
{
	internal sealed class RoutesContainer : IRoutesContainer
	{
		private List<RouteAttribute> _registeredRoutes = null;

		public void RegisterGlobalRoutes()
		{
			if (_registeredRoutes != null)
			{
				return;
			}
			
			_registeredRoutes = new List<RouteAttribute>();

			foreach (Assembly assembly in AppDomain.CurrentDomain.GetAssemblies())
			{
				foreach (Type type in assembly.GetTypes())
				{
					RegisterTypeRoutes(type);
				}
			}
		}

		public void RegisterRoute(RouteAttribute route)
		{
			if (_registeredRoutes == null)
			{
				throw new Exception("RegisterRoutes must be called before registering new routes.");
			}

			if (!ValidateRoute(route))
			{
				Debug.LogError("Trying to register invalid route.");
				return;
			}
			
			_registeredRoutes.Add(route);
		}

		public void RegisterRoutes(IEnumerable<RouteAttribute> routes)
		{
			foreach (var route in routes)
			{
				RegisterRoute(route);
			}
		}

		public void UnRegisterRoute(RouteAttribute route)
		{
			if (_registeredRoutes == null)
			{
				throw new Exception("RegisterRoutes must be called before unregistering any routes.");
			}

			_registeredRoutes.Remove(route);
		}

		public void UnRegisterRoutes(IEnumerable<RouteAttribute> routes)
		{
			foreach (var route in routes)
			{
				UnRegisterRoute(route);
			}
		}

		public IEnumerable<RouteAttribute> GetMatchingRoutes(RequestContext ctx)
		{
			foreach (var route in _registeredRoutes)
			{
				Match match = route.Route.Match(ctx.Path);
				if (!match.Success)
				{
					continue;
				}

				if (!route.Methods.IsMatch(ctx.Request.HttpMethod))
				{
					continue;
				}

				ctx.Match = match;

				yield return route;
			}
		}

		public void ForEach(Action<RouteAttribute> cbk)
		{
			if (cbk == null)
			{
				return;
			}

			foreach (RouteAttribute registeredRoute in _registeredRoutes)
			{
				cbk.Invoke(registeredRoute);
			}
		}

		private void RegisterTypeRoutes(Type type)
		{
			// TODO: Only static methods supported for now
			foreach (var methodInfo in type.GetMethods(BindingFlags.Public | BindingFlags.Static))
			{
				var attrs = methodInfo.GetCustomAttributes(typeof(RouteAttribute), true) as RouteAttribute[];
				if (attrs == null || attrs.Length == 0)
				{
					continue;
				}

				var cbk = Delegate.CreateDelegate(typeof(RouteAttribute.Callback), methodInfo, false) as RouteAttribute.Callback;
				if (cbk == null)
				{
					Debug.LogError($"Method {type}. {methodInfo.Name} takes the wrong arguments for a console route.");
					continue;
				}

				foreach (var route in attrs)
				{
					if (!ValidateRoute(route))
					{
						Debug.LogError($"Method {type}. {methodInfo.Name} needs a valid route regexp.");
						continue;
					}

					route.Cbk = cbk;
					_registeredRoutes.Add(route);
				}
			}	
		}

		private bool ValidateRoute(RouteAttribute route)
		{
			return route != null && route.IsValid;
		}
	}
}
