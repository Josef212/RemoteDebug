﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Threading;
using UnityEngine;

namespace RemoteDebug.Server
{
	internal sealed class RemoteDebugRunner : IRemoteDebugRunner
	{
		private Queue<RequestContext> _requestsQueue = new Queue<RequestContext>();
		private IRoutesContainer _routesContainer = null;
		private Thread _mainThread = null;

		public RemoteDebugRunner(IRoutesContainer container, Thread mainThread)
		{
			_routesContainer = container;
			_mainThread = mainThread;
		}
		
		public void Run()
		{
			while (_requestsQueue.Count > 0)
			{
				RequestContext request;
				lock (_requestsQueue)
				{
					request = _requestsQueue.Dequeue();
				}
				
				HandleRequest(request);
			}
		}

		public void HandleRequest(RequestContext ctx)
		{
			try
			{
				bool handle = false;

				foreach (var route in _routesContainer.GetMatchingRoutes(ctx))
				{
					if (!route.RunOnMainThread && Thread.CurrentThread != _mainThread)
					{
						lock (_requestsQueue)
						{
							_requestsQueue.Enqueue(ctx);
						}

						return;
					}

					route.Cbk(ctx);
					handle = !ctx.Pass;
					if (handle)
					{
						break;
					}
				}

				if (!handle)
				{
					ctx.Response.StatusCode = (int)HttpStatusCode.NotFound;
					ctx.Response.StatusDescription = "Not Found";
				}
			}
			catch (Exception e)
			{
				ctx.Response.StatusCode = (int)HttpStatusCode.InternalServerError;
				ctx.Response.StatusDescription = $"Fatal error:\n{e.Message}";
				Debug.LogException(e);
			}
			
			ctx.Response.OutputStream.Close();
		}
	}
}
