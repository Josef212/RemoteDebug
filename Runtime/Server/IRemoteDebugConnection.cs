namespace RemoteDebug.Server
{
	public interface IRemoteDebugConnection
	{
		bool Active { get; }
		void Start();
		void Stop();
		void Close();
	}
}