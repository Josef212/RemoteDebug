using System;
using System.Net;
using UnityEngine;

namespace RemoteDebug.Server
{
	internal sealed class HttpRemoteDebugConnection : IRemoteDebugConnection
	{
		private int _port;
		private Action<RequestContext> _requestHandler;
		private HttpListener _listener;

		public bool Active => _listener?.IsListening ?? false;
		
		public HttpRemoteDebugConnection(int port, Action<RequestContext> requestHandler)
		{
			_port = port;
			_requestHandler = requestHandler;
			
			Debug.Log($"Starting RemoteDebug server on port: {_port}");
			_listener = new HttpListener();
			_listener.Prefixes.Add($"http://*:{_port}/");
		}

		public void Start()
		{
			_listener.Start();
			_listener.BeginGetContext(ListenerCallback, null);
		}

		public void Stop()
		{
			_listener.Stop();
		}

		public void Close()
		{
			_listener.Close();
		}

		private void ListenerCallback(IAsyncResult result)
		{
			var ctx = _listener.EndGetContext(result);
			_requestHandler?.Invoke(new RequestContext(ctx));

			if (_listener.IsListening)
			{
				_listener.BeginGetContext(ListenerCallback, null);
			}
		}
	}
}