using System.Threading;

namespace RemoteDebug.Server
{
	public interface IRemoteDebugRunner
	{
		void Run();
		void HandleRequest(RequestContext ctx);
	}
}