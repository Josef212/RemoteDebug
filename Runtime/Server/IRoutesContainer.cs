using System;
using System.Collections.Generic;
using RemoteDebug.Attributes;

namespace RemoteDebug.Server
{
	public interface IRoutesContainer
	{
		void RegisterGlobalRoutes();
		void RegisterRoute(RouteAttribute route);
		void RegisterRoutes(IEnumerable<RouteAttribute> route);
		void UnRegisterRoute(RouteAttribute route);
		void UnRegisterRoutes(IEnumerable<RouteAttribute> routes);
		IEnumerable<RouteAttribute> GetMatchingRoutes(RequestContext ctx);
		void ForEach(Action<RouteAttribute> cbk);
	}
}