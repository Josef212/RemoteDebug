﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Threading;
using RemoteDebug.Attributes;
using UnityEngine.Networking;

namespace RemoteDebug.Server
{
	public class FilesHandler
	{
		private static string _fileRoot = default;
        private static Dictionary<string, string> _fileTypes = new Dictionary<string, string>
        {
            {"js",   "application/javascript"},
            {"json", "application/json"},
            {"jpg",  "image/jpeg" },
            {"jpeg", "image/jpeg"},
            {"gif",  "image/gif"},
            {"png",  "image/png"},
            {"css",  "text/css"},
            {"htm",  "text/html"},
            {"html", "text/html"},
            {"ico",  "image/x-icon"},
        };
		
		public static void RegisterFileHandlers(string fileRoot, IRoutesContainer routesContainer)
		{
			_fileRoot = fileRoot;
			
            var pattern = string.Format("({0})", string.Join("|", _fileTypes.Select(x => x.Key).ToArray()));
            var downloadRoute = new RouteAttribute(string.Format(@"^/download/(.*\.{0})$", pattern));
            var fileRoute = new RouteAttribute(string.Format(@"^/(.*\.{0})$", pattern));

			var needsRequest = _fileRoot.Contains("://");
			Action<RequestContext, bool> cbk = FileHandler;
			if (needsRequest)
			{
				cbk = RequestFileHandler;
			}
			
			downloadRoute.Cbk = ctx => cbk(ctx, true);
			fileRoute.Cbk = ctx => cbk(ctx, false);
			
			routesContainer.RegisterRoute(downloadRoute);
			routesContainer.RegisterRoute(fileRoute);
		}

		private static void FindFileType(RequestContext ctx, bool download, out string path, out string type)
		{
			path = Path.Combine(_fileRoot, ctx.Match.Groups[1].Value);
			
			var ext = Path.GetExtension(path).ToLower().TrimStart(new char[]{'.'});
			if (download || !_fileTypes.TryGetValue(ext, out type))
			{
				type = "application/octet-stream";
			}
		}
		
		private static void FileHandler(RequestContext ctx, bool download)
		{
			FindFileType(ctx, download, out string path, out string type);

			if (File.Exists(path))
			{
				ctx.Response.WriteFile(path, type, download);
			}
			else
			{
				ctx.Pass = true;
			}
		}

		private static void RequestFileHandler(RequestContext ctx, bool download)
		{
			FindFileType(ctx, download, out string path, out string type);

			var req = new UnityWebRequest(path, ctx.Request.HttpMethod);
			req.downloadHandler = new DownloadHandlerBuffer();
			req.SendWebRequest();

			while (!req.isDone)
			{
				Thread.Sleep(0);
			}

			if (string.IsNullOrEmpty(req.error))
			{
				ctx.Response.ContentType = type;
				if (download)
				{
					ctx.Response.AddHeader("Content-dosposition", $"attachment; filename={Path.GetFileName(path)}");
				}
				
				ctx.Response.WriteBytes(req.downloadHandler.data);
				return;
			}

			if (req.error.StartsWith("Couldn't open file"))
			{
				ctx.Pass = true;
			}
			else
			{
				ctx.Response.StatusCode = (int)HttpStatusCode.InternalServerError;
				ctx.Response.StatusDescription = $"Fatal error:\n{req.error}";
			}
		}
	}
}
