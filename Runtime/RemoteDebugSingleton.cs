using UnityEngine;

namespace RemoteDebug
{
	public sealed class RemoteDebugSingleton : RemoteDebug
	{
		private static RemoteDebug _instance = null;
		private static bool _destroyed = false;

		public static RemoteDebug Instance
		{
			get
			{
				if (_destroyed)
				{
					Debug.LogError("Remote debug instance already destroyed.");
					return null;
				}

				_instance = FindObjectOfType<RemoteDebugSingleton>();
				if (_instance == null)
				{
					var go = new GameObject("[Singleton] Remote Debug");
					_instance = go.AddComponent<RemoteDebugSingleton>();
				}

				if (_instance == null)
				{
					Debug.LogError("An error has ocurred while getting RemoteDebug instance.");
				}

				return _instance;
			}
		}

		protected override void Awake()
		{
			_destroyed = false;
			_instance = this;
			DontDestroyOnLoad(gameObject);

			base.Awake();
		}

		protected override void OnDestroy()
		{
			_destroyed = true;
			base.OnDestroy();
		}
	}
}