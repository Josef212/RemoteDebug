using System.Collections.Generic;
using RemoteDebug.Attributes;
using RemoteDebug.Server;
using UnityEngine;

namespace RemoteDebug
{
	public abstract class RemoteDebugService : ScriptableObject
	{
		[SerializeField]
		[Tooltip("Defines if the service should be enabled on start or not.")] 
		private bool _enabled;
		
		protected RemoteDebug _remoteDebug;
		protected IRoutesContainer _routesContainer;

		public bool EnabledDefault => _enabled;
		
		protected abstract IEnumerable<RouteAttribute> ServiceRoutes { get; }
		
		public abstract void PostInitService();
		protected abstract void OnPreInitService();
		protected abstract void OnCleanUp();

		public void PreInitService(RemoteDebug remoteDebug, IRoutesContainer container)
		{
			_remoteDebug = remoteDebug;
			_routesContainer = container;
			
			OnPreInitService();
			_routesContainer.RegisterRoutes(ServiceRoutes);
		}

		public void CleanUpService()
		{
			_routesContainer.UnRegisterRoutes(ServiceRoutes);
			OnCleanUp();
		}
	}
}