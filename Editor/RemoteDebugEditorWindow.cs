using RemoteDebug.Console;
using RemoteDebug.Server;
using UnityEditor;
using UnityEngine;

namespace RemoteDebug.Editor
{
	public class RemoteDebugEditorWindow : EditorWindow
	{
		private string _commandInput = default;
		private RemoteDebug _remoteDebug = null;
		
		[MenuItem("RemoteDebug/Commands")]
		private static void ShowWindow()
		{
			var window = GetWindow<RemoteDebugEditorWindow>();
			window.titleContent = new GUIContent("RemoteDebug.Commands");
			window.Show();
		}

		private void OnGUI()
		{
			if (!Application.isPlaying)
			{
				return;
			}

			_remoteDebug = EditorGUILayout.ObjectField("RemoteDebug", _remoteDebug, typeof(RemoteDebug), true) as RemoteDebug;

			if (_remoteDebug == null)
			{
				return;
			}
			
			RoutesGUI(_remoteDebug.RoutesContainer);
			var console = _remoteDebug.GetRemoteService<ConsoleService>()?.Console;
			CommandsGUI(console?.Container);
			
			GUILayout.Space(10f);
			GUILayout.Label("Testing commands splitting", EditorStyles.boldLabel);
			_commandInput = GUILayout.TextField(_commandInput);

			using (new GUILayout.HorizontalScope())
			{
				if (GUILayout.Button("Test without args"))
				{
					TestCommandSplitWithoutArgs(console, _commandInput);
				}
				
				if (GUILayout.Button("Test with args"))
				{
					TestCommandSplitWithArgs(console, _commandInput);
				}
			}
		}

		private void RoutesGUI(IRoutesContainer container)
		{
			GUILayout.Label("Routes registered", EditorStyles.boldLabel);
			
			if (container == null)
			{
				GUILayout.Label("No routes found.");
				return;
			}

			container.ForEach(x =>
			{
				GUILayout.Label($"[{x.Methods}] - {x.Route}");
			});
		}

		private void CommandsGUI(ICommandsContainer container)
		{
			GUILayout.Label("Commands registered", EditorStyles.boldLabel);

			if (container == null)
			{
				GUILayout.Label("No commands found.");
				return;
			}
			
			container.ForEach(x =>
			{
				GUILayout.Label($"{x.Command}");
			});
		}

		private void TestCommandSplitWithoutArgs(Console.Console console, string input)
		{
			console.Container.SplitCommandWithoutArgsTest(input, out var splitCommands);
			Debug.Log($"{input}: {string.Join(" _ ", splitCommands)}");
		}

		private void TestCommandSplitWithArgs(Console.Console console, string input)
		{
			console.Container.SplitCommandWithArgsTest(input, out var splitCommands, out var args);
			Debug.Log($"{input}: {string.Join(" _ ", splitCommands)} | {string.Join(" _ ", args)}");
		}
	}
}
