using System.IO;
using UnityEditor;
using UnityEngine;

namespace RemoteDebug.Editor
{
	public class RemoteDebugStreamingAssets : EditorWindow
	{
		[MenuItem("RemoteDebug/RemoteDebug")]
		private static void ShowWindow()
		{
			var window = GetWindow<RemoteDebugStreamingAssets>();
			window.titleContent = new GUIContent("RemoteDebug");
			window.Show();
		}

		private const string _metaExtension = ".meta";

		private string _pluginPath = "RemoteDebug";
		private string _filesRoot = "RemoteDebug";

		private GUIContent _pluginSrcContent;
		private GUIContent _filesRootContent;

		private void OnEnable()
		{
			_pluginSrcContent = new GUIContent("Plugins source", "Relative path to the RemoteDebug plugin.");
			_filesRootContent = new GUIContent("Files root", "Relative path to RemoteDebug StreamingAssets folder.");
		}

		private void OnGUI()
		{
			EditorGUILayout.HelpBox(
				@"Use this tool to automatically copy RemoteDebug streaming assets to the project directory. Note that [FilesRoot] field should match the RemoteDebug.cs field.", 
				MessageType.Info
				);
			
			EditorGUILayout.Space();
			
			_pluginPath = EditorGUILayout.TextField(_pluginSrcContent, _pluginPath);
			_filesRoot = EditorGUILayout.TextField(_filesRootContent, _filesRoot);
			if (GUILayout.Button("Update StreamingAssets"))
			{
				UpdateStreamingAssets();
			}
		}

		private void UpdateStreamingAssets()
		{
			Debug.Log("Updating streaming assets...");
			
			var srcPath = Path.Combine(Application.dataPath, _filesRoot, "StreamingAssets/RemoteDebug");
			var dstPath = Path.Combine(Application.streamingAssetsPath, _filesRoot);

			Debug.Log($"Src: {srcPath}");
			
			EditorUtility.DisplayProgressBar("Updating RemoteDebug streaming assets.", "", 0f);

			var files = Directory.GetFiles(srcPath);
			for (var i = 0; i < files.Length; ++i)
			{
				var file = files[i];
				
				// file Has the hole path
				var filename = Path.GetFileName(file);
				var ext = Path.GetExtension(file);

				if (ext == _metaExtension)
				{
					continue;
				}

				EditorUtility.DisplayProgressBar("Updating RemoteDebug streaming assets.", file, (i + 1) / (float)files.Length);
				
				var dstFilePath = Path.Combine(dstPath, filename);

				if (File.Exists(dstFilePath))
				{
					File.Delete(dstFilePath);
				}
				
				File.Copy(file, dstFilePath);
			}
			
			EditorUtility.ClearProgressBar();
			AssetDatabase.Refresh();

			Debug.Log($"Updated files from [{srcPath}] to [{dstPath}]");
		}
	}
}